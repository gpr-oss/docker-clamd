#!/usr/bin/env sh

if [ ! -f /var/lib/clamav/main.cvd ]
then
    echo "No database found; launching freshclam..."
    /usr/local/bin/freshclam
fi

exec $@
